//
//  ViewController.swift
//  CoreAnimation
//
//  Created by umar on 10/24/17.
//  Copyright © 2017 iOS. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var passwordTextField: shakingTextField!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    func viewTapped() {
        view.endEditing(true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTapped)))
        
        passwordTextField.delegate = self
        

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.addPulse))
        avatarImageView.isUserInteractionEnabled = true
        tapGestureRecognizer.numberOfTapsRequired = 1
        avatarImageView.addGestureRecognizer(tapGestureRecognizer)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func addPulse() {
        let pulse = Pulsing(numberOfPulses: 1, radius: 110, position: avatarImageView.center)
        pulse.animationDuration = 0.0
        pulse.backgroundColor = UIColor.red.cgColor
        
        self.view.layer.insertSublayer(pulse, below: avatarImageView.layer)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        passwordTextField.shake()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

